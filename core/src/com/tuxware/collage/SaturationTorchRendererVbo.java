package com.tuxware.collage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.util.ArrayList;

public class SaturationTorchRendererVbo extends LuminanceTorchRendererVbo {

    public SaturationTorchRendererVbo(PerspectiveCamera cam, ArrayList<Triangle> triangles, ArrayList<Texture> textures) {
        super(cam, triangles, textures);

        float distance = -cam.position.z;

        String texturedVertexShaderString   = Gdx.files.internal("vertex-regular-torch.glsl").readString();
        String texturedFragmentShaderString = Gdx.files.internal("fragment-regular-collage-textured-graytorch.glsl").readString();
        texturedFragmentShaderString = texturedFragmentShaderString.replaceAll("__AMPLITUDE__FACTOR__", String.valueOf(Math.pow(distance, 2.0)) );

        ShaderProgram.pedantic = true;
        shaderProgram = new ShaderProgram(texturedVertexShaderString,texturedFragmentShaderString);

        this.getUniformLocations();
    }
}
