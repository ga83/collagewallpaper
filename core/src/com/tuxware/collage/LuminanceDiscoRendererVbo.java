package com.tuxware.collage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LuminanceDiscoRendererVbo extends TriangleRenderer {

    private static final int TEXTURED_NUMBER_OF_POSITION_COMPONENTS = 3;	            // xyz
    private static final int TEXTURED_NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS = 2;   // xy
    private static final int TEXTURED_NUMBER_OF_CENTER_COMPONENTS = 3;	            // xyz
    private static final int TEXTURED_NUMBER_OF_VERTEX_COMPONENTS = TEXTURED_NUMBER_OF_POSITION_COMPONENTS + TEXTURED_NUMBER_OF_CENTER_COMPONENTS + TEXTURED_NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS;



    // generalised vbo data
    private ArrayList<VBOData> texturedVboDatas = new ArrayList<VBOData>();

    // the position that the camera is looking at
    private Vector3 lookAt;
    private Vector3 camDirection = new Vector3();
    private Vector3 lightpos2 = new Vector3();


    @Override
    public void resetCamera(PerspectiveCamera cam, Vector3 lookAt) {
        this.cam = cam;
        this.lookAt = lookAt;
    }

    public void render(ArrayList<Triangle> triangles) {

        // TODO: maybe let user select background color
        Gdx.gl.glClearColor(0.0f,0.0f,0.0f,1.0f);

        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_COLOR_BUFFER_BIT);


        // draw textured triangles
        shaderProgram.begin();


        // set the light position at a point between the camera and the lookat, at a distance of camera.z.
        // the z coordinate of light position is (remains) 0.
        // it's not the exact intersection of the ray and the plane, but it's close enough, and less cpu time.
        this.camDirection.set (this.lookAt.cpy().sub(this.cam.position).nor() );
        this.lightpos2.set( cam.position.cpy().add(this.camDirection.scl(cam.position.z)) );
        this.lightPos[0] = this.lightpos2.x;
        this.lightPos[1] = this.lightpos2.y;

        shaderProgram.setUniform3fv(LightPosLocation,lightPos,0,3);
        shaderProgram.setUniformMatrix(u_ProjTransLocation, cam.combined);

        for(int i = 0; i < this.texturedVboDatas.size(); i++) {
            VBOData vboData = this.texturedVboDatas.get(i);
            VertexBufferObject vbo = vboData.vbo;
            vbo.bind(shaderProgram);
            vboData.texture.bind();
            Gdx.gl.glDrawArrays(GL20.GL_TRIANGLES, 0, vbo.getNumVertices());
        }

        shaderProgram.end();


    }


    protected void addTexturedTriangleToVbo(Triangle triangle, boolean topTriangle, VBOData vboData, float centerX, float centerY, float centerZ) {

        // vertex 1
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.x;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.z;

        // texture uv vboIndex
        if(topTriangle == true) {
            vboData.vboVerts[vboData.vboIndex++] = 0;
            vboData.vboVerts[vboData.vboIndex++] = 0;
        } else {
            vboData.vboVerts[vboData.vboIndex++] = 1;
            vboData.vboVerts[vboData.vboIndex++] = 1;
        }

        // center
        vboData.vboVerts[vboData.vboIndex++] = centerX;
        vboData.vboVerts[vboData.vboIndex++] = centerY;
        vboData.vboVerts[vboData.vboIndex++] = centerZ;


        // vertex 2
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.x;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.z;

        // texture uv vboIndex
        if(topTriangle == true) {
            vboData.vboVerts[vboData.vboIndex++] = 1;
            vboData.vboVerts[vboData.vboIndex++] = 0;
        } else {
            vboData.vboVerts[vboData.vboIndex++] = 0;
            vboData.vboVerts[vboData.vboIndex++] = 1;
        }

        // center
        vboData.vboVerts[vboData.vboIndex++] = centerX;
        vboData.vboVerts[vboData.vboIndex++] = centerY;
        vboData.vboVerts[vboData.vboIndex++] = centerZ;


        // vertex 3
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.x;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.z;

        // texture uv vboIndex
        if(topTriangle == true) {
            vboData.vboVerts[vboData.vboIndex++] = 0;
            vboData.vboVerts[vboData.vboIndex++] = 1;
        } else {
            vboData.vboVerts[vboData.vboIndex++] = 1;
            vboData.vboVerts[vboData.vboIndex++] = 0;
        }

        // center
        vboData.vboVerts[vboData.vboIndex++] = centerX;
        vboData.vboVerts[vboData.vboIndex++] = centerY;
        vboData.vboVerts[vboData.vboIndex++] = centerZ;

    }

    @Override
    public void finalize() {
        for(int i = 0; i < this.texturedVboDatas.size(); i++) {
            this.texturedVboDatas.get(i).texture.dispose();
            this.texturedVboDatas.get(i).vbo.dispose();
        }

        this.shaderProgram.dispose();
    }


    public LuminanceDiscoRendererVbo(PerspectiveCamera cam, ArrayList<Triangle> triangles, ArrayList<Texture> textures, Vector3 lookAt) {

        this.lightPos[2] = 0.0f;

        this.lookAt = lookAt;

        this.cam = cam;
        float distance = cam.position.z;

        // initialise textured shader program
        String texturedVertexShaderString   = Gdx.files.internal("vertex-regular-disco.glsl").readString();

        float amp = distance / 2.0f;

        texturedVertexShaderString = texturedVertexShaderString.replaceAll("__AMPLITUDE__FACTOR__", String.valueOf(amp) );
        texturedVertexShaderString = texturedVertexShaderString.replaceAll("__SIDE_LENGTH__", String.valueOf(Constants.COLUMN_WIDTH / 2) );

        String texturedFragmentShaderString = Gdx.files.internal("fragment-regular-collage-textured-disco.glsl").readString();

        ShaderProgram.pedantic = true;
        shaderProgram = new ShaderProgram(texturedVertexShaderString,texturedFragmentShaderString);

        // separate triangles into different VBOs based on texture

        // map of textured triangles
        Map<Texture,ArrayList<Triangle>> textureMap = new HashMap<Texture,ArrayList<Triangle>>();

        // map of untextured triangles, for eg for frames / sides of pictures
        ArrayList<Triangle> untexturedTriangles = new ArrayList<Triangle>();


        int texturesSize = textures.size();

        // in this scheme, we ignore any textures in the triangles (ie Triangle.texture), because the textures are assigned in this class.
        // moreover, they are not assigned to each individial triangle, but to the whole vbo which contains the list of triangles.
        // we separate triangles into textued and untextured, because they will be drawn very differently... ie with texture, or just a color.
        for (int i = 0; i < triangles.size(); i += 2) {

            Triangle triangle = triangles.get(i);

            // if textured, add to textured map , otherwise add to untextured map
            if(triangle.textured == true) {

                Texture texture = null;

                // if the scheme didn't put in a texture yet, assign one now
                if(triangle.texture == null) {
                    texture = textures.get(Utilities.rnd.nextInt(texturesSize));
                } else {
                    texture = triangle.texture;
                }

                // top triangle
                Triangle triangleTop = triangles.get(i);

                // bottom triangle
                Triangle triangleBottom = triangles.get(i + 1);

                // if texture key is not there yet, create a new triangle list identified by this texture
                // else, add the triangle to the existing triangle list
                if (textureMap.containsKey(texture) == false) {
                    textureMap.put(texture, new ArrayList<Triangle>());
                }

                textureMap.get(texture).add(triangleTop);
                textureMap.get(texture).add(triangleBottom);
            } else {
                // top triangle
                Triangle triangleTop = triangles.get(i);

                // bottom triangle
                Triangle triangleBottom = triangles.get(i + 1);

                untexturedTriangles.add(triangleTop);
                untexturedTriangles.add(triangleBottom);
            }

        }

        // a lot happens in here!
        createTexturedVboDatas(textureMap);


        String log = shaderProgram.getLog();
        if (shaderProgram.isCompiled() == false) {
            throw new GdxRuntimeException(log);
        }
        System.out.println("Shader Log: " + log);

        this.getUniformLocations();

    }


    private void createTexturedVboDatas(Map<Texture, ArrayList<Triangle>> textureMap) {
        // iterate through all the keys of the map we've just created, and create a vbodata for each texture (key)
        for (Map.Entry<Texture, ArrayList<Triangle>> entry :textureMap.entrySet()) {

            Texture texture = entry.getKey();
            ArrayList<Triangle> trianglesOfTexture = entry.getValue();

            // i think it should be this, but trying above for now
            float[] vboVerts = new float[trianglesOfTexture.size() * 3 * TEXTURED_NUMBER_OF_VERTEX_COMPONENTS];

            VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
            VertexAttribute vT = new VertexAttribute( VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoords");
            VertexAttribute vC = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_center");
            VertexAttributes vboVAs = new VertexAttributes(new VertexAttribute[] { vA, vT, vC });
            VertexBufferObject vbo = new VertexBufferObject(false, vboVerts.length / TEXTURED_NUMBER_OF_VERTEX_COMPONENTS, vboVAs);

            VBOData vboData = new VBOData(trianglesOfTexture, vboVerts, vbo, texture);
            this.texturedVboDatas.add(vboData);
        }


        // add the vbodata's trianglelist to the vbodata's vbo
        for(int i = 0; i < this.texturedVboDatas.size(); i++) {

            VBOData vboData = this.texturedVboDatas.get(i);
            ArrayList<Triangle> triangleList = vboData.triangleList;

            for(int j = 0; j < triangleList.size(); j+= 2) {

                float[] centres = this.getCentres(triangleList.get(j), triangleList.get(j +1));

                float centerX = centres[0];
                float centerY = centres[1];
                float centerZ = centres[2];

                // add top triangle
                Triangle triangle1 = triangleList.get(j);
                addTexturedTriangleToVbo(triangle1, true, vboData, centerX, centerY, centerZ);

                // add bottom triangle
                Triangle triangle2 = triangleList.get(j + 1);
                addTexturedTriangleToVbo(triangle2, false, vboData, centerX, centerY, centerZ);
            }

            // this setVertices() does a copy operation... so it MUST come after the calls to addTexturedTriangleToVbo() !!!!
            vboData.vbo.setVertices(vboData.vboVerts, 0, vboData.vboVerts.length);
        }
    }

    private float[] getCentres(Triangle t1, Triangle t2) {
        float cx = (t1.v1.x + t1.v2.x + t1.v3.x + t2.v1.x + t2.v2.x + t2.v3.x) / 6.0f;
        float cy = (t1.v1.y + t1.v2.y + t1.v3.y + t2.v1.y + t2.v2.y + t2.v3.y) / 6.0f;
        float cz = (t1.v1.z + t1.v2.z + t1.v3.z + t2.v1.z + t2.v2.z + t2.v3.z) / 6.0f;
        return new float[] { cx, cy, cz };
    }



}
