package com.tuxware.collage;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

public class Triangle {

    public Vector3 v1;
    public Vector3 v2;
    public Vector3 v3;
    public Vector3 normal;
    // only used when the texture is assigned in the scheme, eg in constant-width, or constant-height, otherwise the renderer ignores it and assigns its own texture
    public Texture texture;
    public boolean textured;



    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;

        // this is never used yet, so leave it out to save battery
        //this.normal = Utilities.getScreenFacingNormal(this, null);
    }


    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, boolean textured) {
        this(v1,v2,v3);
        this.textured = textured;
    }

    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3, boolean textured, Texture texture) {
        this(v1,v2,v3);
        this.textured = textured;
        this.texture = texture;
    }


}
