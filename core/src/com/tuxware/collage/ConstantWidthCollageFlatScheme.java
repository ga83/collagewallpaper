package com.tuxware.collage;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

// this one can cover a much larger area because it has far fewer triangles to draw. the pictures have no thickness, just the top is drawn.
public class ConstantWidthCollageFlatScheme implements MeshScheme {

    private ArrayList<Triangle> triangles; // = new ArrayList<Triangle>();

    private static final float Z_COORDINATE_BASE    = 0.0f;

    private ArrayList<Texture> textures = null;

    // list of columns... each element contains data about how tall each image in the column is, and the margin between each.
    private ArrayList<ColumnData> columnDataList;

    // must be even for now.
    public static final int NUMBER_OF_COLUMNS = 30;

    private static final int COLUMN_WIDTH = Constants.COLUMN_WIDTH;
    private static final float HORIZONTAL_MARGIN = 15.0f;
    private static final float VERTICAL_MARGIN = 15.0f;

    // multiplying by number of columns just for a ballpark figure
    private static final float TOTAL_HEIGHT = COLUMN_WIDTH * NUMBER_OF_COLUMNS;


    public ConstantWidthCollageFlatScheme(ArrayList<Texture> textures) {
        this.textures = textures;
        constructColumnDatas();
    }

    private void constructColumnDatas() {
        this.columnDataList = new ArrayList<ColumnData>();

        for(int i = 0; i < NUMBER_OF_COLUMNS; i++) {
            ColumnData columnData = new ColumnData(VERTICAL_MARGIN, TOTAL_HEIGHT, COLUMN_WIDTH);
            this.columnDataList.add(columnData);
        }
    }


    @Override
    public ArrayList<Triangle> create() {

        triangles = new ArrayList<Triangle>();

        int firstColumn = -NUMBER_OF_COLUMNS / 2 + 1;
        int lastColumn = NUMBER_OF_COLUMNS / 2;

        int numTextures = this.textures.size();

        float x = firstColumn * COLUMN_WIDTH;

        for(int column = firstColumn, i = 0; column < lastColumn; column++, i++) {

            ColumnData columnData = this.columnDataList.get(i);

            x += COLUMN_WIDTH + HORIZONTAL_MARGIN;

            // not a fixed number of rows, but depends on when we can't add more images
            boolean lastAddSuccess = true;
            Texture lastTexture = null;

            while(lastAddSuccess == true) {
                Texture texture = this.textures.get(Utilities.rnd.nextInt(numTextures));

                // if multiple textures are available, make sure this one is not a repeat of the one above it
                if(this.textures.size() > 1) {
                    while (lastTexture == texture) {
                        texture = this.textures.get(Utilities.rnd.nextInt(numTextures));
                    }
                }

                lastAddSuccess = columnData.addImage(texture);

                lastTexture = texture;
            }

            // set the coordinates of every image in the row, so we can pull them out and generate triangles
            columnData.setImageCoordinates();

            // generate triangles based on the image coordinates just calculated
            for(int j = 0; j < columnData.imageDataList.size(); j++) {

                ImageData imageData = columnData.imageDataList.get(j);

             //   float x = column * COLUMN_WIDTH;
                float y = -TOTAL_HEIGHT / 2 + imageData.y1 ; // shift it above 0 into negative y
                float width = COLUMN_WIDTH;
                float height = imageData.imageHeight;
                Texture texture = imageData.texture;

                // System.out.println("aaa adding rectangle " + x + ", " + y + ", " + width + ", " + height + ", " + texture);
                generateRectangle(x, y, width, height, texture);
            }

        }

        return triangles;
    }


    // in this scheme, textures must be assigned in the scheme, not in the renderer, because the textures are tied to the images' width and height
    private void generateRectangle(float x, float y, float width, float height, Texture texture) {

        // vertices
        Vector3 topLeft     = new Vector3(x, y + height, Z_COORDINATE_BASE);
        Vector3 topRight    = new Vector3(x + width, y + height, Z_COORDINATE_BASE);
        Vector3 bottomLeft  = new Vector3(x, y, Z_COORDINATE_BASE);
        Vector3 bottomRight = new Vector3(x + width, y, Z_COORDINATE_BASE);

        Triangle triangle;

        // roof with texture
        triangle = new Triangle(topLeft.cpy(), topRight.cpy(), bottomLeft.cpy(), true, texture);
        this.triangles.add(triangle);

        triangle = new Triangle(bottomRight.cpy(), bottomLeft.cpy(), topRight.cpy(), true, texture);
        this.triangles.add(triangle);
    }

    @Override
    public ArrayList<Triangle> update(float x, float y) {
        return null;
    }
}
