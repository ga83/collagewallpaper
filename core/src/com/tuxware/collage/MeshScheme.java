package com.tuxware.collage;


import java.util.ArrayList;

public interface MeshScheme {

    ArrayList<Triangle> create();                  // initial creation of mesh
    ArrayList<Triangle> update(float x, float y);  // subsequent update of mesh, given the camera location

}
