package com.tuxware.collage;

import com.badlogic.gdx.graphics.Color;

public class KirraPointLightFadeOut extends KirraPointLight {



    public KirraPointLightFadeOut(float positionX, float positionY, Color color, float dt) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.color = color;
        this.dt = dt;
    }

    public KirraPointLightFadeOut(float positionX, float positionY, Color color, float dt, float timeAlive, float speedY, float interpolateTime, float timeSpan) {
        this(positionX, positionY, color, dt);
        this.timeAlive = timeAlive;
        this.interpolateTime = interpolateTime;
        this.timeSpan = timeSpan;
        this.speedY = speedY;

//        System.out.println("aaa zzz out timeAlive1: " + this.timeAlive + ", " + this.timeSpan);
    }



    @Override
    public float update() {
        this.timeAlive += this.dt;
        this.positionY -= this.speedY * this.dt;
        return this.timeAlive;
    }

    @Override
    public float getAmplitude() {
        float x = this.timeSpan - this.timeAlive;
        float amp = 1.0f - (InterpolationOperations.cosxZeroToOnePi(x, 0, this.interpolateTime) + 1.0f) * 0.5f;

        //System.out.println("aaa update out amp: " + amp + ", " + this.timeAlive);

        return amp;
    }

}
