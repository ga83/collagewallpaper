package com.tuxware.collage;

import com.badlogic.gdx.graphics.Color;

import java.util.Random;

public abstract class KirraPointLight {

//    public static float TIME_SPAN = 4.0f;
  //  public static float INTERPOLATE_TIME = 1.5f;

    public float dt;

    // geometry units
    public float positionX;
    public float positionY;

    public float timeAlive;
    public float timeSpan;
    public float interpolateTime;

    public Color color;

    public float speedY;

    protected static final Random rnd = new Random();

    abstract float update();
    abstract float getAmplitude();
}
