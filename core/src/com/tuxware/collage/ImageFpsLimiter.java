package com.tuxware.collage;

import com.badlogic.gdx.Gdx;

public class ImageFpsLimiter implements FpsLimiter {

    public void limitFps() {

        // there is always a noticeable jitter when limiting fps (with this technique at least)... turning off for now
        /*
            try {
                Thread.sleep(CollageWallpaper.sleepDuration);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        if (Gdx.graphics.getRawDeltaTime() > 1.0f / 59.0f) {
            CollageWallpaper.sleepDuration -= 1;

            if (CollageWallpaper.sleepDuration < 0){
                CollageWallpaper.sleepDuration = 0;
            }

        } else if (Gdx.graphics.getRawDeltaTime() < 1.0f / 61.0f) {
            CollageWallpaper.sleepDuration += 1;
        }
        */
    }

}
