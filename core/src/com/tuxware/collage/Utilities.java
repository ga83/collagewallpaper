package com.tuxware.collage;

import com.badlogic.gdx.math.Vector3;

import java.util.Random;

public class Utilities {

    private static Vector3 screenFacingNormal = new Vector3(0,0,-1);

    public static Random rnd = new Random();

    public static Vector3 getScreenFacingNormal(Triangle t1, Vector3 specifiedNormal) {

        Vector3 arm1 = t1.v1.cpy().sub(t1.v2).nor();
        Vector3 arm2 = t1.v3.cpy().sub(t1.v2).nor();
        Vector3 normal1 = arm1.cpy().crs(arm2).nor();
        Vector3 normal2 = normal1.cpy().scl(-1f).nor();

        float normal1dot;
        float normal2dot;

        if(specifiedNormal == null) {
            normal1dot = normal1.dot(screenFacingNormal);
            normal2dot = normal2.dot(screenFacingNormal);
        } else {
            normal1dot = normal1.dot(specifiedNormal);
            normal2dot = normal2.dot(specifiedNormal);
        }


        if(normal1dot > normal2dot) {
            return normal1;
        } else {
            return normal2;
        }
    }


}
