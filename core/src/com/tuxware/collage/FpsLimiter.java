package com.tuxware.collage;

interface FpsLimiter {
    void limitFps();
}
