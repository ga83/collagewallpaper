package com.tuxware.collage;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public abstract class TriangleRenderer {

    protected float[] lightPos = new float[] { 0.0f, 0.0f, 0.0f };	// this is relative to the camera (screen). it needs to be transformed somehow to be camera-independent.
    protected PerspectiveCamera cam;
    protected float[] globalNormal = new float[] { 0.0f, 0.0f, 1.0f };


    public abstract void resetCamera(PerspectiveCamera cam, Vector3 lookAt);
    public abstract void render(ArrayList<Triangle> triangles);

    protected ShaderProgram shaderProgram;

    protected int LightPosLocation;
    protected int u_ProjTransLocation;
    protected int GNormalLocation;
    protected int NumPointLightsLocation;
    protected int PointLightPositionsLocation;
    protected int PointLightColorsLocation;
    protected int PointLightAmpsLocation;


    protected void getUniformLocations() {
        LightPosLocation = this.shaderProgram.getUniformLocation("LightPos");
        u_ProjTransLocation = this.shaderProgram.getUniformLocation("u_projTrans");
        GNormalLocation = this.shaderProgram.getUniformLocation("GNormal");
        NumPointLightsLocation = this.shaderProgram.getUniformLocation("numPointLights");

        PointLightPositionsLocation = shaderProgram.getUniformLocation("pointLightPositions[0]");
        PointLightColorsLocation = shaderProgram.getUniformLocation("pointLightColors[0]");
        PointLightAmpsLocation = shaderProgram.getUniformLocation("pointLightAmps[0]");
    }

}
