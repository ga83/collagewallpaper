package com.tuxware.collage;

import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;

class ColumnData {

    // a list of each image's height.
    public ArrayList<ImageData> imageDataList;

    // the margin height we are aiming for, but probably will not hit exactly.
    public float marginHeight;

    // total height of column passed in by higher level
    private float totalHeightTarget;

    // column width passed in from higher level
    private final float columnWidth;


    public ColumnData(float marginHeight, float totalColumnHeight, float columnWidth) {
        this.imageDataList = new ArrayList<ImageData>();
        this.marginHeight = marginHeight;
        this.totalHeightTarget = totalColumnHeight;
        this.columnWidth = columnWidth;
    }

    // returns true if the add was allowed, false if not allowed
    public boolean addImage(Texture texture) {

        // we will resize the width and height according to the specifiedWidth supplied
        float textureWidth = texture.getWidth();
        float textureHeight = texture.getHeight();
        float resizedHeight = (this.columnWidth / textureWidth) * textureHeight;

        boolean success = tryToAddImageToList(texture, this.columnWidth, resizedHeight);
        return success;
    }

    // old, chooses a margin height which could be quite different from the side (vertical) margins. but it looks better with margins that match the side margins' thickness.
    // this method is used to calculate the
    public boolean tryToAddImageToList(Texture texture, float resizedWidth, float resizedHeight) {

        float cumulativeHeightOfImages = 0.f;
        int cumulativeNumberOfImages = 0;

        // find the total height of the images, we will add the margins' cumulative height later
        for(int i = 0; i < imageDataList.size(); i++) {
            cumulativeHeightOfImages += imageDataList.get(i).imageHeight;
            cumulativeNumberOfImages ++;
        }

        // must include the newly added image as well
        cumulativeHeightOfImages += resizedHeight;
        cumulativeNumberOfImages ++;

        // the number of margins is one less than the number of images, because the images are "fenceposts"
        int numMargins = cumulativeNumberOfImages - 1;
        float cumulativeMarginHeight = numMargins * this.marginHeight;

        float totalHeight = cumulativeMarginHeight + cumulativeHeightOfImages;

        // if we haven't gone over the target total height yet, return true so that caller knows it can add another image. otherwise, return false.
        if(totalHeight < this.totalHeightTarget) {
            ImageData imageData = new ImageData(resizedWidth, resizedHeight, texture);
            this.imageDataList.add(imageData);
            return true;
        } else {
            return false;
        }
    }


    // should only need to be called once, after all the images have been added
    public void setImageCoordinates() {

        // find the height first of all, so that we can calculate the offset and include it
        float cumulativeHeight = 0.0f;

        for(int i = 0; i < this.imageDataList.size(); i++) {
            ImageData imageData = imageDataList.get(i);
            cumulativeHeight += imageData.imageHeight + this.marginHeight;
        }

        float offset = 0 - (cumulativeHeight - this.totalHeightTarget) / 2.0f;

        float currentY = offset;

        for(int i = 0; i < this.imageDataList.size(); i++) {
            ImageData imageData = imageDataList.get(i);
            imageData.y1 = currentY;
            currentY += this.marginHeight + imageData.imageHeight;
        }

    }


}




