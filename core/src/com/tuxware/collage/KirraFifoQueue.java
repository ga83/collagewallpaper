package com.tuxware.collage;

/**
 * This uses less CPU instructions and is faster than both an array list (ArrayList) and a standard fifo queue (ArrayBlockingQueue) for two reasons:
 * It has no locks and is not thread-safe.
 * It doesn't copy items upon (every) removal - only when the backing array is full.
 * It is therefore a standard space-time tradeoff.
 * It is much faster than ArrayList, and slightly faster than ArrayBlockingQueue when configured correctly.
 */
public class KirraFifoQueue<T> {

    // extra memory, over and above the number of items the caller wants to store (ie container size). it provides more performance by prolonging the need to copy memory upon remove() calls.
    // in production, for even less cpu instruction overhead, remove this variable and just replace its uses with a hardcoded value.
//    private int extraReservedMemorySize;

    // fixed size set by user.
    private Object[] backingArray;

    // this keeps track of the index of item which will pop out next. always lower than insertionEndIndex.
    private int removalEndIndex;

    // this keeps track of the index which the next insert will go into. always higher than removalEndIndex.
    private int insertionEndIndex;


    /**
     * KirraFifoQueue constructor
     * @param containerSize the maximum number of items the caller wants to store in the list at once. caller must not try to store more than this.
     * 10 is good but there's no problem in changing it. at about 100, the wall time performance is about the same as ArrayBlockingQueue.
     */
    public KirraFifoQueue(int containerSize) {
        this.backingArray = new Object[containerSize + 2000];
    }

    public void set(int index, T item) {
        this.backingArray[this.removalEndIndex + index] = (T)item;
    }

    public T peek(int index) {
        return (T)(this.backingArray[this.removalEndIndex + index]);
    }

    public int size() {
        return this.insertionEndIndex - this.removalEndIndex;
    }


    public void remove() {
//        this.backingArray[this.removalEndIndex] = null;
        this.removalEndIndex ++;

        if(this.removalEndIndex == 2000) {
            int numItems = this.insertionEndIndex - this.removalEndIndex;
            System.arraycopy(this.backingArray, 2000, this.backingArray, 0, numItems);
            this.insertionEndIndex = numItems;
            this.removalEndIndex = 0;
        }
    }

    public void add(T item) {
        this.backingArray[this.insertionEndIndex] = item;
        this.insertionEndIndex++;
    }


}
