package com.tuxware.collage;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;


public class CollageWallpaper extends ApplicationAdapter implements ApplicationListener {

	private static final float X_MIN_CAMERA_BOUNCE = -10000;
	private static final float X_MAX_CAMERA_BOUNCE =  10000;
	private static final float Y_MIN_CAMERA_BOUNCE = -10000;
	private static final float Y_MAX_CAMERA_BOUNCE =  10000;

	private static final float X_MIN_LOOKAT_BOUNCE = (float) (X_MIN_CAMERA_BOUNCE * 0.75);
	private static final float X_MAX_LOOKAT_BOUNCE = (float) (X_MAX_CAMERA_BOUNCE * 0.75);
	private static final float Y_MIN_LOOKAT_BOUNCE = (float) (Y_MIN_CAMERA_BOUNCE * 0.75);
	private static final float Y_MAX_LOOKAT_BOUNCE = (float) (Y_MAX_CAMERA_BOUNCE * 0.75);

	private static final float X_MIN_CAMERA_RESET = X_MIN_CAMERA_BOUNCE * 2.0f;
	private static final float X_MAX_CAMERA_RESET = X_MAX_CAMERA_BOUNCE * 2.0f;
	private static final float Y_MIN_CAMERA_RESET = Y_MIN_CAMERA_BOUNCE * 2.0f;
	private static final float Y_MAX_CAMERA_RESET = Y_MAX_CAMERA_BOUNCE * 2.0f;

	private static final float X_MIN_LOOKAT_RESET = X_MIN_LOOKAT_BOUNCE * 2.0f;
	private static final float X_MAX_LOOKAT_RESET = X_MAX_LOOKAT_BOUNCE * 2.0f;
	private static final float Y_MIN_LOOKAT_RESET = Y_MIN_LOOKAT_BOUNCE * 2.0f;
	private static final float Y_MAX_LOOKAT_RESET = Y_MAX_LOOKAT_BOUNCE * 2.0f;


	private static final float LOOKAT_SPEED = 300;   // units per second

	private static final float MAX_CAMERA_STEER_ANGULAR_VELOCITY = 5;
	private static final float MAX_LOOKAT_STEER_ANGULAR_VELOCITY = 5;
	private static final int LOOK_AT_DISTANCE = -8000;
	public float cameraSpeed;
	private Vector3 upVector;


	private enum ResizeStrategy {
		CROP, BLACKBORDERS, BLURBORDERS, NONE
	}


	private FPSLogger fps1;

    private ArrayList<Triangle> triangles;	// just a reference to the triangle list stored in the mesh scheme, because the scheme is responsible for updating it

	private PerspectiveCamera cam;

	public Random rnd = new Random();

	private Vector3 lookAtPosition;
	private Vector3 cameraPosition;

	private Vector3 lookAtVelocity;
	public Vector3 cameraVelocity;

    private float cameraSteerAngularVelocity = 0;
    private float lookAtSteerAngularVelocity = 0;

    private Vector3 rotationAxis = new Vector3(0,0,1);

	private Preferences prefs;

	public static boolean preferencesChanged = false;

	private long currentFrame = 0;

	private String shape;

	private TriangleRenderer triangleRenderer;

	private float fov;
	private String lightingStyle;
	private float cameraDistance;

	private ArrayList<Texture> textures;
	private String imagePath;
	private FpsLimiter fpsLimiter;
	private ResizeStrategy resizeStrategy;
    private int minImageResolution;
	private int maxImagesLoad;
	private int textureQuality;

	private static HashSet<String> imageExtensions = new HashSet<String>(Arrays.asList(new String[] { "png", "jpg", "bmp" }));



	public static void restart() {
		System.exit(0);
	}


	@Override
	public void create() {
		initialisePreferences();
		generateMesh();

		this.fps1 = new FPSLogger();
		this.upVector = new Vector3(0.0f, 1.0f, 0.0f);
	}

	private void initialisePreferences() {

		prefs = Gdx.app.getPreferences("prefs");

		String shape = prefs.getString("shape");
		String fov = prefs.getString("fov");
		String lightingStyle = prefs.getString("lightingstyle");
		String cameraDistance = prefs.getString("cameradistance");
		String resizeStrategy = prefs.getString("resizestrategy");
		String minImageResolution = prefs.getString("minimageresolution");
		String maxImagesLoad = prefs.getString("maximagesload");
		String textureQuality = prefs.getString("texturequality");
		String cameraSpeed = prefs.getString("cameraspeed");
		String aaSamples = prefs.getString("aasamples");


		if(shape.isEmpty() == true || shape == null) {
			prefs.putString("shape","constantwidthcollageflat");
		}

		if(fov.isEmpty() == true || fov == null) {
			prefs.putString("fov","90");
		}

		if(lightingStyle.isEmpty() == true || lightingStyle == null) {
			prefs.putString("lightingstyle","glowing");
		}

		if(cameraDistance.isEmpty() == true || cameraDistance == null) {
			prefs.putString("cameradistance","3000");
		}

		if(resizeStrategy.isEmpty() == true || resizeStrategy == null) {
			prefs.putString("resizestrategy","crop");
		}

        if(minImageResolution.isEmpty() == true || minImageResolution == null) {
            prefs.putString("minimageresolution","500");
        }

		if(maxImagesLoad.isEmpty() == true || maxImagesLoad == null) {
			prefs.putString("maximagesload","10");
		}

		if(textureQuality.isEmpty() == true || textureQuality == null) {
			prefs.putString("texturequality","768");
		}

		if(cameraSpeed.isEmpty() == true || cameraSpeed == null) {
			prefs.putString("cameraspeed","300.0");
		}

		if(aaSamples.isEmpty() == true || aaSamples == null) {
			prefs.putString("aasamples","2");
		}

		prefs.flush();
	}


	private void setupDepth() {
		// TODO: depth testing is not working properly. it seems to half work (it does make a difference turning it on), but still some distant faces get drawn on top of closer ones.
		// TODO: seems like we can't have blending and depth testing at the same time. shouldn't need blending though.
		Gdx.gl.glDepthMask(true);
		Gdx.gl.glDisable(GL20.GL_BLEND);
		Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glDepthRangef(0.0f,1.0f);
		Gdx.gl.glClearDepthf(4.0f);
		Gdx.gl.glClearColor(0, 0, 0.0f, 1);
		Gdx.gl.glDepthFunc(GL20.GL_LESS);
		Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
	}

	private void setupNoDepth() {
		Gdx.gl.glDepthMask(false);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_ONE, GL20.GL_ONE);
		Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glClearColor(0, 0, 0.0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}


	@Override
	public void render() {

		if(preferencesChanged == true) {
			generateMesh();
			preferencesChanged = false;
		}

		fpsLimiter.limitFps();

		float dt = Gdx.graphics.getRawDeltaTime();

		currentFrame ++;

		// stuff we can save on cpu by only doing every so often instead of every frame
		if(currentFrame % 60 == 0) {
			randomiseLookAtVelocity();
			randomiseCameraVelocity();
			checkOutOfBounds();
		}

		this.updateCameraAndLookAtPosition(dt);

        cam.position.x = cameraPosition.x;
		cam.position.y = cameraPosition.y;
		cam.position.z = cameraPosition.z;

		cam.lookAt(lookAtPosition.x, lookAtPosition.y, lookAtPosition.z);
		cam.up.set(this.upVector);



		cam.update(true);

		triangleRenderer.render(triangles);

		fps1.log();
	}

	@Override
	public void resume() {
		System.out.println("===== gg resume");
		cam.viewportWidth = Gdx.graphics.getWidth();
		cam.viewportHeight = Gdx.graphics.getHeight();
	}

	private void generateMesh() {

		prefs = Gdx.app.getPreferences("prefs");

		// TODO: voronoi is not easy to use, but it's possible.
		// TODO: for example, only some points (in the middle of the image mostly) are actually surrounded by edges
		// TODO: on all sides. we have to generate some edges manually to make every point, especially the ones
		// TODO: near the edges, be surrounded by lines.
		// TODO: for now, not using voronoi. try generate a pyramid instead.
		// TODO: https://robertlovespi.net/2014/06/16/five-versions-of-a-tessellation-using-squares-and-equilateral-triangles/

		shape = prefs.getString("shape");
		fov = Float.valueOf(prefs.getString("fov"));
		lightingStyle = prefs.getString("lightingstyle");
		cameraDistance = Float.valueOf(prefs.getString("cameradistance"));
		imagePath = prefs.getString("imagepath") + "/";
        minImageResolution = Integer.valueOf(prefs.getString("minimageresolution"));
		maxImagesLoad = Integer.valueOf(prefs.getString("maximagesload"));
		textureQuality = Integer.valueOf(prefs.getString("texturequality"));
		cameraSpeed = Float.valueOf(prefs.getString("cameraspeed"));


		boolean doResize = shape.equals("squarecollageflat") == true;


		if(doResize) {
			if (prefs.getString("resizestrategy").equals("crop") == true) {
				this.resizeStrategy = ResizeStrategy.CROP;
			} else if (prefs.getString("resizestrategy").equals("blackborders") == true) {
				this.resizeStrategy = ResizeStrategy.BLACKBORDERS;
			} else if (prefs.getString("resizestrategy").equals("blurborders") == true) {
				this.resizeStrategy = ResizeStrategy.BLURBORDERS;
			}
		} else {
			this.resizeStrategy = ResizeStrategy.NONE;
		}

		this.fpsLimiter = new ImageFpsLimiter();

		ArrayList<String> textureFilePaths = null;

		// don't allow root directory because it's too slow to recurse through
		if(imagePath.equals("/") == false) {
			textureFilePaths = getTextureFilePaths(imagePath);
		}

		resetCamera(false);

		// create scheme, ie geometry... as of now, only one shape
		if(shape.equals("squarecollageflat") == true) {
			loadTexturesFromDisk(textureFilePaths, resizeStrategy);
			SquareDiscCollageFlatScheme scheme = new SquareDiscCollageFlatScheme(textures);
			triangles = scheme.create();
		} else if(shape.equals("constantwidthcollageflat") == true) {
			loadTexturesFromDisk(textureFilePaths, resizeStrategy);
			ConstantWidthCollageFlatScheme scheme = new ConstantWidthCollageFlatScheme(textures);
			triangles = scheme.create();
		}

		// select renderer
		if(lightingStyle.equals("glowing") == true) {
			triangleRenderer = new GlowingRendererVbo(cam, triangles, textures);
			setupDepth();
		} else if(lightingStyle.equals("torch") == true) {
			triangleRenderer = new LuminanceTorchRendererVbo(cam, triangles, textures);
			setupDepth();
		} else if(lightingStyle.equals("disco") == true) {
			triangleRenderer = new LuminanceDiscoRendererVbo(cam, triangles, textures, this.lookAtPosition);
			setupDepth();
		} else if(lightingStyle.equals("saturationtorch") == true) {
			triangleRenderer = new SaturationTorchRendererVbo(cam, triangles, textures);
			setupDepth();
		} else if(lightingStyle.equals("saturationdisco") == true) {
			triangleRenderer = new SaturationDiscoRendererVbo(cam, triangles, textures, this.lookAtPosition);
			setupDepth();
		} else if(lightingStyle.equals("whitesparkles") == true) {
			triangleRenderer = new WhiteSparklesRendererVbo(cam, triangles, textures, this.lookAtPosition, this);
			setupDepth();
		} else if(lightingStyle.equals("coloredsparkles") == true) {
			triangleRenderer = new ColoredSparklesRendererVbo(cam, triangles, textures, this.lookAtPosition, this);
			setupDepth();
		}
	}

	private void resetCamera(boolean resetRenderer) {
		this.initialiseCameraPositionAndVelocity();
		this.initialiseLookAtPositionAndVelocity();
		this.initialisePerspectiveCamera();

		if(resetRenderer == true) {
			triangleRenderer.resetCamera(this.cam, this.lookAtPosition);
		}
	}

	private void initialisePerspectiveCamera() {

		cam = new PerspectiveCamera(fov, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		cam.viewportWidth = Gdx.graphics.getWidth();
		cam.viewportHeight = Gdx.graphics.getHeight();

		cam.lookAt(lookAtPosition.x, lookAtPosition.y, lookAtPosition.z);
		cam.near = 100f;
		cam.far = 182000f;

		cam.position.x = cameraPosition.x;
		cam.position.y = cameraPosition.y;
		cam.position.z = cameraPosition.z;

		cam.update(true);


	}


	private static ArrayList<String> getTextureFilePaths(String imagePath) {

		FileHandle dirHandle = Gdx.files.absolute(imagePath);
		FileHandle[] list = dirHandle.list();
		ArrayList<String> textureFilePaths = new ArrayList<String>();

		for(int i = 0; i < list.length; i++) {
			FileHandle fileHandle = list[i];
			String fileOrFolderPath = imagePath + fileHandle.name();
			boolean isDirectory = fileHandle.isDirectory();
			boolean isImage = imageExtensions.contains(fileHandle.extension().toLowerCase());

			if(isDirectory == true) {
				// this is a folder, so look inside it recursively
				fileOrFolderPath += "/";
				ArrayList<String> textureFilePathsInsideFolder = getTextureFilePaths(fileOrFolderPath);
				textureFilePaths.addAll(textureFilePathsInsideFolder);

			} else if(isDirectory == false && isImage == true) {
				// this is just a file, so add it to list
				textureFilePaths.add(fileOrFolderPath);
			}
		}

		// shuffle list, because only a certain number can be loaded in memory, and we don't want the same images loaded every time
		Collections.shuffle(textureFilePaths);

		return textureFilePaths;
	}


	private void loadTexturesFromDisk(ArrayList<String> texturePaths, ResizeStrategy resizeStrategy) {

		if(this.textures != null) {
			while(this.textures.size() > 0) {
				Texture texture = this.textures.get(0);
				this.textures.remove(0);
				texture.dispose();
			}
		} else {
			this.textures = new ArrayList<Texture>();
		}

		// if user selected no paths yet, just put the one warning texture in there
		if(texturePaths == null) {
			setErrorTexture();
			return;
		} else {
			System.out.println("aaa texture paths IS NOT NULL");
		}

		boolean loadingLimitHit = false;

		for(int i = 0; i < texturePaths.size() && loadingLimitHit == false; i++) {

			FileHandle imageFile = Gdx.files.absolute(texturePaths.get(i));

			// TODO: come up with a criteria which perhaps takes into account memory used instead of number of files loaded
			if(this.textures.size() == maxImagesLoad) {
				loadingLimitHit = true;
			}

			if(imageFile != null) {

				try {
					Pixmap pixmapOriginalSize = new Pixmap(imageFile);

					int width = pixmapOriginalSize.getWidth();
					int height = pixmapOriginalSize.getHeight();

					if(width < minImageResolution || height < minImageResolution) {
						// don't load the image if it doesn't pass the minimum resolution requirement
					    continue;
                    }


					// the scale depends on the resize strategy.
					// ie, if adding borders, the longest side needs to be below the max height or width.
					// if cropping, only the shortest side needs to be below max height or width.
					float scale = getScale(width, height, resizeStrategy);

					int reducedWidth = (int) (width / scale);
					int reducedHeight = (int) (height / scale);

					// make copy of pixmap, which is smaller
					Pixmap pixmapScaled = new Pixmap(reducedWidth,reducedHeight, Pixmap.Format.RGBA8888);
					pixmapScaled.drawPixmap(pixmapOriginalSize, 0, 0, width, height, 0, 0, reducedWidth, reducedHeight);

					Pixmap pixmapScaledAndCropped;

					// if not square, decide how to crop or add borders to the image. if square, just return original pixmap
					if(this.resizeStrategy != ResizeStrategy.NONE && reducedHeight != reducedWidth) {
						if (resizeStrategy == ResizeStrategy.CROP) {
							pixmapScaledAndCropped = getCroppedPixmap(pixmapScaled);
							pixmapScaled.dispose();
						} else if (resizeStrategy == ResizeStrategy.BLACKBORDERS) {
							pixmapScaledAndCropped = getBordersPixmap(pixmapScaled);
							pixmapScaled.dispose();
						} else { // ie - resizeStrategy == ResizeStrategy.BLURBORDERS {
							pixmapScaledAndCropped = getBlurBordersPixmap(pixmapScaled);
							pixmapScaled.dispose();
						}
					} else {
						pixmapScaledAndCropped = pixmapScaled;
					}

					Texture resizedTexture = new Texture(pixmapScaledAndCropped, true);


					// TODO: the parameters here have a large effect on image quality but also performance. maybe they should be user selectable if performance is an issue for some.
					resizedTexture.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.Linear);


					pixmapOriginalSize.dispose();

					// TODO: IF opengl context losses (upon pause and resume) are frequent, we shouldn't dispose the texture here.
					// TODO: in fact, we should keep the Pixmap around so that Texture objects can be recreated from them on resume.
					// TODO: this step is necessary in the case of context loss because Textures created from Pixmaps are not "managed" textures like ones loaded from files
					// TODO: supposedly, opengl context loss should be increasingly rare so this may not be necessary. read this:
					// TODO: https://badlogicgames.com/forum/viewtopic.php?f=11&t=20757
					// TODO: of course, if opengl context loss is very rare, we should go ahead and dispose the pixmaps, because they take up several hundred mb in total.
					// TODO: also read:
					// TODO: https://www.badlogicgames.com/wordpress/?p=1073
					// TODO: also read:
					// TODO: https://www.badlogicgames.com/wordpress/?p=367
					pixmapScaledAndCropped.dispose();

					this.textures.add(resizedTexture);
				}
				// catch primarily to catch images which can't load for any reason at all... eg too large.
				catch (GdxRuntimeException e) {
					System.out.println("aaa caught exception on file " + texturePaths.get(i) + ", not loading");
				}
			}
		}

		if(this.textures.size() == 0) {
			setErrorTexture();
		}

		return;
	}

	private void setErrorTexture() {
		FileHandle noFolderSelected = Gdx.files.internal("selectfolder.png");
		Pixmap selectFolderPixmap = new Pixmap(noFolderSelected);
		Texture selectFolderTexture = new Texture(selectFolderPixmap);
		selectFolderPixmap.dispose();
		this.textures = new ArrayList<Texture>();
		this.textures.add(selectFolderTexture);
	}


	private Pixmap getBordersPixmap(Pixmap pixmapScaled) {
		Pixmap pixmapWithBorders;

		if(pixmapScaled.getWidth() > pixmapScaled.getHeight()) {
			int increasedHeight = pixmapScaled.getWidth();
			int heightDifference = increasedHeight - pixmapScaled.getHeight();
			int yOffset = heightDifference / 2;
			pixmapWithBorders = new Pixmap(pixmapScaled.getWidth(), increasedHeight, Pixmap.Format.RGBA8888);
			pixmapWithBorders.setColor(com.badlogic.gdx.graphics.Color.BLACK);
			pixmapWithBorders.fill();
			pixmapWithBorders.drawPixmap(pixmapScaled,0,yOffset);

		} else if(pixmapScaled.getHeight() > pixmapScaled.getWidth()) {
			int increasedWidth = pixmapScaled.getHeight();
			int widthDifference = increasedWidth - pixmapScaled.getWidth();
			int xOffset = widthDifference / 2;
			pixmapWithBorders = new Pixmap(increasedWidth, pixmapScaled.getHeight() , Pixmap.Format.RGBA8888);
			pixmapWithBorders.setColor(com.badlogic.gdx.graphics.Color.BLACK);
			pixmapWithBorders.fill();
			pixmapWithBorders.drawPixmap(pixmapScaled,xOffset,0);
		} else {
			pixmapWithBorders = pixmapScaled;
		}

		return pixmapWithBorders;
	}


	private Pixmap getCroppedPixmap(Pixmap pixmapScaled) {
		Pixmap pixmapScaledAndCropped;

		if(pixmapScaled.getWidth() > pixmapScaled.getHeight()) {
			int croppedReducedWidth = pixmapScaled.getHeight();
			int widthDifference = pixmapScaled.getWidth() - croppedReducedWidth;
			int xOffset = 0 - widthDifference / 2;

			pixmapScaledAndCropped = new Pixmap(croppedReducedWidth, pixmapScaled.getHeight(), Pixmap.Format.RGBA8888);
			pixmapScaledAndCropped.drawPixmap(pixmapScaled,xOffset,0);

		} else if(pixmapScaled.getHeight() > pixmapScaled.getWidth()) {
			int croppedReducedHeight = pixmapScaled.getWidth();
			int heightDifference = pixmapScaled.getHeight() - croppedReducedHeight;
			int yOffset = 0 - heightDifference;
			pixmapScaledAndCropped = new Pixmap(pixmapScaled.getWidth(), croppedReducedHeight, Pixmap.Format.RGBA8888);
			pixmapScaledAndCropped.drawPixmap(pixmapScaled,0,yOffset);
		} else {
			pixmapScaledAndCropped = pixmapScaled;
		}
		return pixmapScaledAndCropped;
	}

	private Pixmap getBlurBordersPixmap(Pixmap pixmapScaled) {
		return getBordersPixmap(pixmapScaled);
	}

	private float getScale(int width, int height, ResizeStrategy resizeStrategy) {

		float scale;

		if(resizeStrategy == ResizeStrategy.CROP || resizeStrategy == ResizeStrategy.NONE) {
			scale = 1.0f;

			while (
					width / scale > textureQuality ||
					height / scale > textureQuality
			) {
				scale += 0.1f;
			}
		}
		else { // resizeStrategy == ResizeStrategy.BLACKBORDERS or resizeStrategy == ResizeStrategy.BLURBORDERS
			scale = 1.0f;

			while (
					width / scale > textureQuality &&
					height / scale > textureQuality
			) {
				scale += 0.1f;
			}
		}

		return scale;
	}




	private void initialiseLookAtPositionAndVelocity() {

		float lookAtDistanceToUse = LOOK_AT_DISTANCE;

		lookAtDistanceToUse *= 4.0;

    	Vector2 lookAtPosition2d = new Vector2().setToRandomDirection().scl(rnd.nextFloat() * X_MAX_LOOKAT_BOUNCE);

        lookAtPosition = new Vector3(0, 0, lookAtDistanceToUse);
 		lookAtPosition.x = lookAtPosition2d.x;
 		lookAtPosition.y = lookAtPosition2d.y;

        Vector2 lookAtVelocity2d = new Vector2().setToRandomDirection().scl(LOOKAT_SPEED);

        lookAtVelocity = new Vector3();
        lookAtVelocity.x = lookAtVelocity2d.x;
        lookAtVelocity.y = lookAtVelocity2d.y;
    }

    private void initialiseCameraPositionAndVelocity() {

		float cameraDistanceToUse = this.cameraDistance;

		cameraDistanceToUse /= 4.0;

		cameraPosition = new Vector3(0, 0, cameraDistanceToUse);

        Vector2 cameraVelocity2d = new Vector2().setToRandomDirection().scl(cameraSpeed);

        cameraVelocity = new Vector3();
        cameraVelocity.x = cameraVelocity2d.x;
        cameraVelocity.y = cameraVelocity2d.y;
    }

    private void randomiseCameraVelocity() {

		int sign;

		if(rnd.nextBoolean() == true) {
			sign = 1;
		} else {
			sign = -1;
		}

		cameraSteerAngularVelocity = rnd.nextFloat() * MAX_CAMERA_STEER_ANGULAR_VELOCITY * sign;

		if(cameraSteerAngularVelocity > MAX_CAMERA_STEER_ANGULAR_VELOCITY) {
			cameraSteerAngularVelocity = MAX_CAMERA_STEER_ANGULAR_VELOCITY;
		}
		else if(cameraSteerAngularVelocity < -MAX_CAMERA_STEER_ANGULAR_VELOCITY) {
			cameraSteerAngularVelocity = -MAX_CAMERA_STEER_ANGULAR_VELOCITY;
		}
    }

    // check if the camera or the lookat is out of bounds, and if so,
	// either bounce or reset them both, depending how far out of bounds it / they are
    private void checkOutOfBounds() {

			// check camera position
			if (cameraPosition.x < X_MIN_CAMERA_BOUNCE) {
				// bounce (repeated for all walls below)
				if(cameraVelocity.x < 0) {
					cameraVelocity.x *= -1;
				}
				// too far, reset (repeated for all walls below)
				if(cameraPosition.x < X_MIN_CAMERA_RESET) {
					resetCamera(true);
				}
			}

			if (cameraPosition.x > X_MAX_CAMERA_BOUNCE) {
				if(cameraVelocity.x > 0) {
					cameraVelocity.x *= -1;
				}

				if(cameraPosition.x > X_MAX_CAMERA_RESET) {
					resetCamera(true);
				}
			}

			if (cameraPosition.y < Y_MIN_CAMERA_BOUNCE) {
				if(cameraVelocity.y < 0) {
					cameraVelocity.y *= -1;
				}

				if(cameraPosition.y < Y_MIN_CAMERA_RESET) {
					resetCamera(true);
				}
			}

			if (cameraPosition.y > Y_MAX_CAMERA_BOUNCE) {
				if(cameraVelocity.y > 0) {
					cameraVelocity.y *= -1;
				}

				if(cameraPosition.y > Y_MAX_CAMERA_RESET) {
					resetCamera(true);
				}
			}


			// check lookat position
			if (lookAtPosition.x < X_MIN_LOOKAT_BOUNCE) {
				if (lookAtVelocity.x < 0) {
					lookAtVelocity.x *= -1;
				}

				if (lookAtPosition.x < X_MIN_LOOKAT_RESET) {
					resetCamera(true);
				}

			}

			if (lookAtPosition.x > X_MAX_LOOKAT_BOUNCE) {
				if (lookAtVelocity.x > 0) {
					lookAtVelocity.x *= -1;
				}

				if (lookAtPosition.x > X_MAX_LOOKAT_RESET) {
					resetCamera(true);
				}

			}

			if (lookAtPosition.y < Y_MIN_LOOKAT_BOUNCE) {
				if (lookAtVelocity.y < 0) {
					lookAtVelocity.y *= -1;
				}

				if (lookAtPosition.y < Y_MIN_LOOKAT_RESET) {
					resetCamera(true);
				}

			}

			if (lookAtPosition.y > Y_MAX_LOOKAT_BOUNCE) {
				if (lookAtVelocity.y > 0) {
					lookAtVelocity.y *= -1;
				}

				if (lookAtPosition.y > Y_MAX_LOOKAT_RESET) {
					resetCamera(true);
				}
			}
	}


	private void updateCameraAndLookAtPosition(float dt) {
		cameraVelocity.rotate(rotationAxis, cameraSteerAngularVelocity * dt);
		cameraPosition.add(cameraVelocity.x * dt, cameraVelocity.y * dt, cameraVelocity.z * dt);
		lookAtVelocity.rotate(rotationAxis, lookAtSteerAngularVelocity * dt);
		lookAtPosition.add(lookAtVelocity.x * dt, lookAtVelocity.y * dt, lookAtVelocity.z * dt);
	}

    private void randomiseLookAtVelocity() {

    	int sign;

		if(rnd.nextBoolean() == true) {
			sign = 1;
		} else {
			sign = -1;
		}

		lookAtSteerAngularVelocity = rnd.nextFloat() * MAX_LOOKAT_STEER_ANGULAR_VELOCITY * sign;

		if(lookAtSteerAngularVelocity > MAX_LOOKAT_STEER_ANGULAR_VELOCITY) {
			lookAtSteerAngularVelocity = MAX_LOOKAT_STEER_ANGULAR_VELOCITY;
		}

		else if(lookAtSteerAngularVelocity < -MAX_LOOKAT_STEER_ANGULAR_VELOCITY) {
			lookAtSteerAngularVelocity = -MAX_LOOKAT_STEER_ANGULAR_VELOCITY;
		}

    }

}
