package com.tuxware.collage;

public class InterpolationOperations {

    /**
     * Using function cos(x), interpolates value given to a transformed and clipped value between cos(0) and cos(1 pi).
     * The transformation is such that the minimum corresponds to 0 and the maximum corresponds to pi.
     */
    public static float cosxZeroToOnePi(float x, float min, float max) {
        // clamp
        x = Math.max(x, min);
        x = Math.min(x, max);

        // normalised to 1
        float positionNormalised = (x - min) / (max - min);
        float cosx = (float) Math.cos(positionNormalised * Math.PI);

        return cosx;
    }

    /**
     * Using function cos(x), interpolates value given to a transformed and clipped value between cos(0) and cos(0.5 pi).
     * The transformation is such that the minimum corresponds to 0 and the maximum corresponds to 0.5 pi.
     * If reverse parameter is true, it interpolates in the reverse direction.
     * This would be used when we want a position to begin at full speedY but slow down to a stop, for example.
     */
    public static float cosxZeroToHalfPi(float x, float min, float max, boolean reverse) {
        // clamp
        x = Math.max(x, min);
        x = Math.min(x, max);

        float positionNormalised = (x - min) / (max - min);

        if(reverse == true) {
            positionNormalised = 1.0f - positionNormalised;
        }

        float cosx = (float) Math.cos(positionNormalised * Math.PI * 0.5);

        return cosx;
    }



}
