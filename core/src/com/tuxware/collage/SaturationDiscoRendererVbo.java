package com.tuxware.collage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class SaturationDiscoRendererVbo extends LuminanceDiscoRendererVbo {

    public SaturationDiscoRendererVbo(PerspectiveCamera cam, ArrayList<Triangle> triangles, ArrayList<Texture> textures, Vector3 lookAt) {
        super(cam, triangles, textures, lookAt);

        float distance = cam.position.z;

        // initialise textured shader program
        String texturedVertexShaderString   = Gdx.files.internal("vertex-regular-disco.glsl").readString();

        float amp = distance / 2.0f;

        texturedVertexShaderString = texturedVertexShaderString.replaceAll("__AMPLITUDE__FACTOR__", String.valueOf(amp) );
        texturedVertexShaderString = texturedVertexShaderString.replaceAll("__SIDE_LENGTH__", String.valueOf(Constants.COLUMN_WIDTH / 2) );

        String texturedFragmentShaderString = Gdx.files.internal("fragment-regular-collage-textured-graydisco.glsl").readString();

        ShaderProgram.pedantic = true;
        shaderProgram = new ShaderProgram(texturedVertexShaderString,texturedFragmentShaderString);

        this.getUniformLocations();
    }
}
