package com.tuxware.collage;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

// this one can cover a much larger area because it has far fewer triangles to draw. the pictures have no thickness, just the top is drawn.
public class SquareDiscCollageFlatScheme implements MeshScheme {

    private ArrayList<Triangle> triangles;

    private static final float Z_COORDINATE_BASE    = -100.0f;   // should be 0
    private static final float Z_COORDINATE_TOP = 0.0f;

    private static int SIDE_LENGTH = Constants.COLUMN_WIDTH;


    public SquareDiscCollageFlatScheme(ArrayList<Texture> textures) {
    }


    @Override
    public ArrayList<Triangle> create() {
        triangles = new ArrayList<Triangle>();

        for(int row = -15; row < 15; row++) {
            for (int column = -15; column < 15; column++) {

                float xcentre = SIDE_LENGTH * column;
                float ycentre = SIDE_LENGTH * row;

                generateSquareDisc(xcentre, ycentre);
            }
        }

        return triangles;
    }

    private void generateSquareDisc(float xcentre, float ycentre) {

        float margin = 5;

        // base vertices
        Vector3 baseTopLeft     = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin,  SIDE_LENGTH / 2 + ycentre - margin, Z_COORDINATE_BASE);
        Vector3 baseTopRight    = new Vector3( SIDE_LENGTH / 2 + xcentre - margin,  SIDE_LENGTH / 2 + ycentre - margin, Z_COORDINATE_BASE);
        Vector3 baseBottomLeft  = new Vector3(-SIDE_LENGTH / 2 + xcentre + margin, -SIDE_LENGTH / 2 + ycentre + margin, Z_COORDINATE_BASE);
        Vector3 baseBottomRight = new Vector3( SIDE_LENGTH / 2 + xcentre - margin, -SIDE_LENGTH / 2 + ycentre + margin, Z_COORDINATE_BASE);

        // top vertices
        Vector3 roofTopLeft     = baseTopLeft.cpy();        roofTopLeft.z = Z_COORDINATE_TOP;
        Vector3 roofTopRight    = baseTopRight.cpy();       roofTopRight.z = Z_COORDINATE_TOP;
        Vector3 roofBottomLeft  = baseBottomLeft.cpy();     roofBottomLeft.z = Z_COORDINATE_TOP;
        Vector3 roofBottomRight = baseBottomRight.cpy();    roofBottomRight.z = Z_COORDINATE_TOP;


        Triangle triangle;


        // TODO: SIDES WERE REMOVED!!! ONLY THE ROOF REMAINS! WHY??? PUT SIDES BACK IN!

        // roof with texture
        triangle = new Triangle(roofTopLeft.cpy(), roofTopRight.cpy(), roofBottomLeft.cpy(), true);
        triangles.add(triangle);

        triangle = new Triangle(roofBottomRight.cpy(), roofBottomLeft.cpy(), roofTopRight.cpy(), true);
        triangles.add(triangle);

    }

    @Override
    public ArrayList<Triangle> update(float x, float y) {

        return null;
    }
}
