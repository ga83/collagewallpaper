package com.tuxware.collage;

import com.badlogic.gdx.graphics.Color;

public class KirraPointLightFadeIn extends KirraPointLight {



    public KirraPointLightFadeIn(float positionX, float positionY, Color color, float dt, float lifeTime, float speedY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.color = color;
        this.timeAlive = 0.0f;
        this.timeSpan = lifeTime;
        this.interpolateTime = lifeTime / 20.0f;
        this.dt = dt;
        this.speedY = speedY ;
    }

    public KirraPointLightFadeIn(float positionX, float positionY, Color color, float dt, float lifeTime, float speedY, float interpolationFactor) {
        this(positionX, positionY, color, dt, lifeTime, speedY);
        this.interpolateTime = lifeTime / interpolationFactor;
    }



    @Override
    public float update() {
        this.timeAlive += this.dt;
        this.positionY -= this.speedY * this.dt;
        return this.timeAlive;
    }

    @Override
    public float getAmplitude() {
        float x = this.timeAlive;
        float amp = 1.0f - (InterpolationOperations.cosxZeroToOnePi(x, 0, interpolateTime) + 1.0f) * 0.5f;

        //System.out.println("aaa update in amp: " + amp + ", " + this.timeAlive);

        return amp;
    }


}
