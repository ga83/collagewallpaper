package com.tuxware.collage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.VertexBufferObject;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ColoredSparklesRendererVbo extends TriangleRenderer {

    private static final float DT = 1.0f / 60.0f;

    private static final int TEXTURED_NUMBER_OF_POSITION_COMPONENTS = 3;	            // xyz
    private static final int TEXTURED_NUMBER_OF_NORMAL_COMPONENTS = 0;	            // xyz
    private static final int TEXTURED_NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS = 2;   // xy
    private static final int TEXTURED_NUMBER_OF_VERTEX_COMPONENTS = TEXTURED_NUMBER_OF_POSITION_COMPONENTS + TEXTURED_NUMBER_OF_NORMAL_COMPONENTS + TEXTURED_NUMBER_OF_TEXTURE_COORDINATE_COMPONENTS;

    private static final int UNTEXTURED_NUMBER_OF_POSITION_COMPONENTS = 3;	            // xyz
    private static final int UNTEXTURED_NUMBER_OF_NORMAL_COMPONENTS = 3;	            // xyz
    private static final int UNTEXTURED_NUMBER_OF_VERTEX_COMPONENTS = UNTEXTURED_NUMBER_OF_POSITION_COMPONENTS + UNTEXTURED_NUMBER_OF_NORMAL_COMPONENTS;

    private static final float SCHEME_WIDTH = Constants.COLUMN_WIDTH * ConstantWidthCollageFlatScheme.NUMBER_OF_COLUMNS;
    private static final float SCHEME_HEIGHT = SCHEME_WIDTH;
    private static final int NUMBER_OF_LIGHTS = 20;
    private static final int[] SIGNS = new int[] { -1, 1 };


    // generalised vbo data
    private ArrayList<VBOData> texturedVboDatas = new ArrayList<VBOData>();

    private KirraFifoQueue<KirraPointLight> pointLights = new KirraFifoQueue<KirraPointLight>(NUMBER_OF_LIGHTS);
    private static Random rnd = new Random();

    public static float[] pointLightPositions = new float[NUMBER_OF_LIGHTS * 3];
    public static float[] pointLightColors = new float[NUMBER_OF_LIGHTS * 3];
    public static float[] pointLightAmps = new float[NUMBER_OF_LIGHTS * 1];

//    public static Color[] COLORS = new Color[] { Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN, Color.MAGENTA };
    public static Color[] COLORS = new Color[] { Color.RED, Color.GREEN, Color.BLUE, Color.RED, Color.GREEN, Color.BLUE };

    public long lastAdd;
    private int colorIndex;

    private Vector3 lookAt;
    private Vector3 camDirection = new Vector3();
    private Vector3 lightpos2 = new Vector3();

    private CollageWallpaper collageWallpaper;



    @Override
    public void resetCamera(PerspectiveCamera cam, Vector3 lookAt) {
        this.cam = cam;
        this.lookAt = lookAt;
    }


    private void updatePointLights() {

        /*
        // remove dead particles
        while(this.pointLights.size() > 0) {
            KirraPointLight kpl = this.pointLights.peek(0);
            if(kpl.timeAlive > kpl.timeSpan) {
                System.out.println("aaa zzz removing");
                this.pointLights.remove();
            } else {
                break;
            }
        }
         */

        int lightsSize;
        lightsSize = this.pointLights.size();

        for(int i = 0; i < lightsSize; i++) {
            KirraPointLight kpl = this.pointLights.peek(i);
            kpl.update();

            if(kpl.timeAlive > kpl.timeSpan - kpl.interpolateTime && kpl instanceof KirraPointLightFadeIn) {
                kpl = new KirraPointLightFadeOut(kpl.positionX, kpl.positionY, kpl.color, DT, kpl.timeAlive, kpl.speedY, kpl.interpolateTime, kpl.timeSpan);
                this.pointLights.set(i, kpl);
            }
        }

        // remove dead particles
        while(this.pointLights.size() > 0) {
            KirraPointLight kpl = this.pointLights.peek(0);
            if(kpl.timeAlive > kpl.timeSpan) {
                System.out.println("aaa zzz removing");
                this.pointLights.remove();
            } else {
                break;
            }
        }

        float gap = this.cam.position.z / 10f;
        //System.out.println("aaa gap: " + gap);

        if((this.pointLights.size() < NUMBER_OF_LIGHTS) && (System.currentTimeMillis() > this.lastAdd + (int)gap)) {


            // set the light position at a point between the camera and the lookat, at a distance of camera.z.
            // the z coordinate of light position is (remains) 0.
            // it's not the exact intersection of the ray and the plane, but it's close enough, and less cpu time.
            this.camDirection.set (this.lookAt.cpy().sub(this.cam.position).nor() );
            this.lightpos2.set( cam.position.cpy().add(this.camDirection.scl(cam.position.z)) );
            this.lightPos[0] = this.lightpos2.x;
            this.lightPos[1] = this.lightpos2.y;

            float camVelocityX = this.collageWallpaper.cameraVelocity.x;
            float camVelocityY = this.collageWallpaper.cameraVelocity.y;

            float lifeTime = this.cam.position.z * 2.0f * 0.0007f * 2.0f;
            float speedY = 100.0f + 10.0f * rnd.nextFloat();

            System.out.println("aaa lifetime: " + lifeTime);

            float interpolationDivisor = 6.0f;

            float camAheadX = camVelocityX * lifeTime * 0.5f;
            float camAheadY = camVelocityY * lifeTime * 0.5f;

            float x = this.lightPos[0] + rnd.nextFloat() * this.cam.position.z * 0.5f * SIGNS[rnd.nextInt(2)] + camAheadX;
//            float x = this.lightPos[0] + camAheadX;
            float y = this.lightPos[1] + this.cam.position.z * 0.25f + camAheadY;

            // TODO: colored lights can be used for another renderer, but for now, comment out.
            this.colorIndex = (this.colorIndex + 1) % 6;
            Color color = ColoredSparklesRendererVbo.COLORS[this.colorIndex];


            KirraPointLightFadeIn kirraPointLightFadeIn = new KirraPointLightFadeIn(x, y, color, DT, lifeTime, speedY, interpolationDivisor);
            this.pointLights.add(kirraPointLightFadeIn);

            this.lastAdd = System.currentTimeMillis();
        }


        for (int i = 0; i < this.pointLights.size(); i++) {
            KirraPointLight kirraPointLight = this.pointLights.peek(i);

            ColoredSparklesRendererVbo.pointLightPositions[i * 3] = kirraPointLight.positionX;
            ColoredSparklesRendererVbo.pointLightPositions[i * 3 + 1] = kirraPointLight.positionY;
            ColoredSparklesRendererVbo.pointLightPositions[i * 3 + 2] = cam.position.z / 4.0f;

            ColoredSparklesRendererVbo.pointLightColors[i * 3] = kirraPointLight.color.r;
            ColoredSparklesRendererVbo.pointLightColors[i * 3 + 1] = kirraPointLight.color.g;
            ColoredSparklesRendererVbo.pointLightColors[i * 3 + 2] = kirraPointLight.color.b;

            ColoredSparklesRendererVbo.pointLightAmps[i] = kirraPointLight.getAmplitude() *0.5f ;
        }
    }


    public void render(ArrayList<Triangle> triangles) {

        this.updatePointLights();

        Gdx.gl.glClearColor(0.0f,0.0f,0.0f,1.0f);
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_COLOR_BUFFER_BIT);
        shaderProgram.begin();

        lightPos[0] = cam.position.x;
        lightPos[1] = cam.position.y;

        shaderProgram.setUniform3fv(LightPosLocation, lightPos, 0, 3);
        shaderProgram.setUniformMatrix(u_ProjTransLocation, cam.combined);

        shaderProgram.setUniform3fv(GNormalLocation, globalNormal, 0, 3);


        //System.out.println("aaa size: " + this.pointLights.size());

        shaderProgram.setUniformi(NumPointLightsLocation, this.pointLights.size());
//        shaderProgram.setUniformi(NumPointLightsLocation, 1);


//        shaderProgram.setUniform1fv(PointLightPositionsLocation, pointLightPositions, 0, pointLightPositions.length);
        //      shaderProgram.setUniform1fv(PointLightColorsLocation, pointLightColors, 0, pointLightColors.length);
        //    shaderProgram.setUniform1fv(PointLightAmpsLocation, pointLightAmps, 0, pointLightAmps.length);

        shaderProgram.setUniform1fv("pointLightPositions", pointLightPositions, 0, pointLightPositions.length);
        shaderProgram.setUniform1fv("pointLightColors", pointLightColors, 0, pointLightColors.length);
        shaderProgram.setUniform1fv("pointLightAmps", pointLightAmps, 0, pointLightAmps.length);



        for(int i = 0; i < this.texturedVboDatas.size(); i++) {
            VBOData vboData = this.texturedVboDatas.get(i);
            VertexBufferObject vbo = vboData.vbo;
            vbo.bind(shaderProgram);
            vboData.texture.bind();
            Gdx.gl.glDrawArrays(GL20.GL_TRIANGLES, 0, vbo.getNumVertices());
        }
        shaderProgram.end();
    }


    protected void addTexturedTriangleToVbo(Triangle triangle, boolean topTriangle, VBOData vboData) {
//        Vector3 triangleNormal = triangle.normal;

        // vertex 1
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.x;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.z;


        // texture uv vboIndex
        if(topTriangle == true) {
            vboData.vboVerts[vboData.vboIndex++] = 0;
            vboData.vboVerts[vboData.vboIndex++] = 0;
        } else {
            vboData.vboVerts[vboData.vboIndex++] = 1;
            vboData.vboVerts[vboData.vboIndex++] = 1;
        }

        // vertex 2
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.x;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.z;


        // texture uv vboIndex
        if(topTriangle == true) {
            vboData.vboVerts[vboData.vboIndex++] = 1;
            vboData.vboVerts[vboData.vboIndex++] = 0;
        } else {
            vboData.vboVerts[vboData.vboIndex++] = 0;
            vboData.vboVerts[vboData.vboIndex++] = 1;
        }

        // vertex 3
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.x;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.z;


        // texture uv vboIndex
        if(topTriangle == true) {
            vboData.vboVerts[vboData.vboIndex++] = 0;
            vboData.vboVerts[vboData.vboIndex++] = 1;
        } else {
            vboData.vboVerts[vboData.vboIndex++] = 1;
            vboData.vboVerts[vboData.vboIndex++] = 0;
        }
    }



    protected void addUntexturedTriangleToVbo(Triangle triangle, boolean topTriangle, VBOData vboData) {

        Vector3 triangleNormal = triangle.normal;

/*
        float r = triangle.color.r;
        float g = triangle.color.g;
        float b = triangle.color.b;
*/

        //bottom left vertex
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.x; 			//Position(x, y)
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v1.z;

        /*
        // color
        vboData.vboVerts[vboData.vboIndex++] = r; 	//Color(r, g, b, a)
        vboData.vboVerts[vboData.vboIndex++] = g;
        vboData.vboVerts[vboData.vboIndex++] = b;
        */

        // normal
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.x;
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.y;
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.z;

        //top left vertex
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.x; 			//Position(x, y)
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v2.z;

        /*
        // color
        vboData.vboVerts[vboData.vboIndex++] = r; 	//Color(r, g, b, a)
        vboData.vboVerts[vboData.vboIndex++] = g;
        vboData.vboVerts[vboData.vboIndex++] = b;
        */

        // normal
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.x;
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.y;
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.z;

        //bottom right vertex
        // position
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.x;	 //Position(x, y)
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.y;
        vboData.vboVerts[vboData.vboIndex++] = triangle.v3.z;

        /*
        // color
        vboData.vboVerts[vboData.vboIndex++] = r; 	//Color(r, g, b, a)
        vboData.vboVerts[vboData.vboIndex++] = g;
        vboData.vboVerts[vboData.vboIndex++] = b;
        */

        // normal
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.x;
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.y;
        vboData.vboVerts[vboData.vboIndex++] = triangleNormal.z;
    }


    @Override
    public void finalize() {
        for(int i = 0; i < this.texturedVboDatas.size(); i++) {
            this.texturedVboDatas.get(i).texture.dispose();
            this.texturedVboDatas.get(i).vbo.dispose();
        }

        this.shaderProgram.dispose();
    }


    public ColoredSparklesRendererVbo(PerspectiveCamera cam, ArrayList<Triangle> triangles, ArrayList<Texture> textures, Vector3 lookAt, CollageWallpaper collageWallpaper) {

        this.lightPos[2] = cam.position.z;

        this.cam = cam;

        this.lookAt = lookAt;

        this.collageWallpaper = collageWallpaper;

        float distanceCamera = -cam.position.z;
        float distanceSparkle = distanceCamera / 4.0f;

        // initialise textured shader program
        String texturedVertexShaderString   = Gdx.files.internal("vertex-regular-torch.glsl").readString();
        String texturedFragmentShaderString = Gdx.files.internal("fragment-regular-collage-textured-torch-sparkles.glsl").readString();
        texturedFragmentShaderString = texturedFragmentShaderString.replaceAll("__AMPLITUDE__FACTOR__CAMERA__", String.valueOf(Math.pow(distanceCamera, 2.0) * 0.01) );
        texturedFragmentShaderString = texturedFragmentShaderString.replaceAll("__AMPLITUDE__FACTOR__SPARKLE__", String.valueOf(Math.pow(distanceSparkle, 2.0) * 1.0) );

        ShaderProgram.pedantic = true;
        shaderProgram = new ShaderProgram(texturedVertexShaderString,texturedFragmentShaderString);

        // separate triangles into different VBOs based on texture

        // map of textured triangles
        Map<Texture,ArrayList<Triangle>> textureMap = new HashMap<Texture,ArrayList<Triangle>>();

        // map of untextured triangles, for eg for frames / sides of pictures
        ArrayList<Triangle> untexturedTriangles = new ArrayList<Triangle>();


        int texturesSize = textures.size();

        // in this scheme, we ignore any textures in the triangles (ie Triangle.texture), because the textures are assigned in this class.
        // moreover, they are not assigned to each individial triangle, but to the whole vbo which contains the list of triangles.
        // we separate triangles into textued and untextured, because they will be drawn very differently... ie with texture, or just a color.
        for (int i = 0; i < triangles.size(); i += 2) {

            Triangle triangle = triangles.get(i);

            // if textured, add to textured map , otherwise add to untextured map
            if(triangle.textured == true) {

                Texture texture = null;

                // if the scheme didn't put in a texture yet, assign one now
                if(triangle.texture == null) {
                    texture = textures.get(Utilities.rnd.nextInt(texturesSize));
                } else {
                    texture = triangle.texture;
                }

                // top triangle
                Triangle triangleTop = triangles.get(i);

                // bottom triangle
                Triangle triangleBottom = triangles.get(i + 1);

                // if texture key is not there yet, create a new triangle list identified by this texture
                // else, add the triangle to the existing triangle list
                if (textureMap.containsKey(texture) == false) {
                    textureMap.put(texture, new ArrayList<Triangle>());
                }

                textureMap.get(texture).add(triangleTop);
                textureMap.get(texture).add(triangleBottom);
            } else {
                // top triangle
                Triangle triangleTop = triangles.get(i);

                // bottom triangle
                Triangle triangleBottom = triangles.get(i + 1);

                untexturedTriangles.add(triangleTop);
                untexturedTriangles.add(triangleBottom);
            }

        }



        // a lot happens in here!
        createTexturedVboDatas(textureMap);

        // some stuff happens in here!
        createUntexturedVboData(untexturedTriangles);


        String log = shaderProgram.getLog();
        if (shaderProgram.isCompiled() == false) {
            throw new GdxRuntimeException(log);
        }
        System.out.println("Shader Log: " + log);

        this.getUniformLocations();

    }

    // TODO: we could add separate colors to the side triangles here, to give a bit more fun.
    private void createUntexturedVboData(ArrayList<Triangle> untexturedTriangles) {

        float[] vboVerts = new float[untexturedTriangles.size() * 3 * UNTEXTURED_NUMBER_OF_VERTEX_COMPONENTS];

        VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
        VertexAttribute vN = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_normal" );
        VertexAttributes vboVAs = new VertexAttributes(new VertexAttribute[] { vA, vN });
        VertexBufferObject vbo = new VertexBufferObject(false, vboVerts.length / UNTEXTURED_NUMBER_OF_VERTEX_COMPONENTS, vboVAs);

        VBOData vboData = new VBOData(untexturedTriangles, vboVerts, vbo);

        for(int j = 0; j < untexturedTriangles.size(); j+= 2) {
            // add top triangle
            Triangle triangle1 = untexturedTriangles.get(j);
            addUntexturedTriangleToVbo(triangle1, true, vboData);

            // add bottom triangle
            Triangle triangle2 = untexturedTriangles.get(j + 1);
            addUntexturedTriangleToVbo(triangle2, false, vboData);
        }

        vboData.vbo.setVertices(vboData.vboVerts, 0, vboData.vboVerts.length);

//        this.untexturedVboData = vboData;

    }

    private void createTexturedVboDatas(Map<Texture, ArrayList<Triangle>> textureMap) {
        // iterate through all the keys of the map we've just created, and create a vbodata for each texture (key)
        for (Map.Entry<Texture, ArrayList<Triangle>> entry :textureMap.entrySet()) {

            Texture texture = entry.getKey();
            ArrayList<Triangle> trianglesOfTexture = entry.getValue();

            // i think it should be this, but trying above for now
            float[] vboVerts = new float[trianglesOfTexture.size() * 3 * TEXTURED_NUMBER_OF_VERTEX_COMPONENTS];

            VertexAttribute vA = new VertexAttribute( VertexAttributes.Usage.Position, 3, "a_position" );
            VertexAttribute vT = new VertexAttribute( VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoords");
            VertexAttributes vboVAs = new VertexAttributes(new VertexAttribute[] { vA, vT });
            VertexBufferObject vbo = new VertexBufferObject(false, vboVerts.length / TEXTURED_NUMBER_OF_VERTEX_COMPONENTS, vboVAs);

            VBOData vboData = new VBOData(trianglesOfTexture, vboVerts, vbo, texture);
            this.texturedVboDatas.add(vboData);
        }


        // add the vbodata's trianglelist to the vbodata's vbo
        for(int i = 0; i < this.texturedVboDatas.size(); i++) {

            VBOData vboData = this.texturedVboDatas.get(i);
            ArrayList<Triangle> triangleList = vboData.triangleList;

            for(int j = 0; j < triangleList.size(); j+= 2) {
                // add top triangle
                Triangle triangle1 = triangleList.get(j);
                addTexturedTriangleToVbo(triangle1, true, vboData);

                // add bottom triangle
                Triangle triangle2 = triangleList.get(j + 1);
                addTexturedTriangleToVbo(triangle2, false, vboData);
            }

            // this setVertices() does a copy operation... so it MUST come after the calls to addTexturedTriangleToVbo() !!!!
            vboData.vbo.setVertices(vboData.vboVerts, 0, vboData.vboVerts.length);
        }
    }



}
