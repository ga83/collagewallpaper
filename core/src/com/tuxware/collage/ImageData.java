package com.tuxware.collage;

import com.badlogic.gdx.graphics.Texture;

// represents one image
public class ImageData {

    // x coordinate of top left
    public float x1;

    // y coordinate of top left
    public float y1;

    public float imageHeight;
    public float imageWidth;
    public Texture texture;


    public ImageData(float width, float height, Texture texture) {
        this.imageWidth = width;
        this.imageHeight = height;
        this.texture = texture;
    }
}
