// TODO: THIS VERTEX SHADER IS FOR UNTEXTURED TRIANGLES, WHICH WE DON'T USE AT THE MOMENT, IF EVER. IT INCLUDES COLOR AND NORMAL VARYINGS TO SEND TO FRAG SHADER.

//our attributes
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_texCoords;

//our camera matrix
uniform mat4 u_projTrans;


//send the color out to the fragment shader
varying vec4 vColor;
varying vec3 vNormal;
varying vec3 fragPosition;
varying vec2 texCoord;


void main() {

	vColor = a_color;
    vNormal = a_normal;

	gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);

	fragPosition = gl_Position.xyz;

	texCoord = a_texCoords;
}
