#ifdef GL_ES
precision highp float;
#endif


#define MAX_POINT_LIGHTS 20


uniform sampler2D texture;
varying vec2 texCoord;

// lighting variables
varying vec3 FragPositionWorldSpace;
varying vec3 LightPositionWorldSpace;


// point lights
uniform int numPointLights;
uniform float pointLightPositions   [3 * MAX_POINT_LIGHTS];
uniform float pointLightColors      [3 * MAX_POINT_LIGHTS];
uniform float pointLightAmps        [1 * MAX_POINT_LIGHTS];

// since all photos face exactly the same way, we don't need per-vertex normals, just one global uniform one.
uniform vec3 GNormal;


void main() {

    // on-camera point light
    float onCameraTotal = __AMPLITUDE__FACTOR__CAMERA__ / pow ( (distance(LightPositionWorldSpace, FragPositionWorldSpace)) , 2.0);


    // diffuse and specular of sparkles
    vec4 diffuseTotal = vec4(0.0, 0.0, 0.0, 0.0);
//    vec4 specularTotal = vec4(0.0, 0.0, 0.0, 0.0);

    for(int i = 0, j = 0; i < numPointLights * 3; i += 3, j++) {
        // common
        vec3 lightPos = vec3(pointLightPositions[i], pointLightPositions[i + 1], pointLightPositions[i + 2]);
        vec3 backToLightSourcePoint = normalize(lightPos - FragPositionWorldSpace);
        float pointLightAmp = pointLightAmps[j];

        // diffuse
        float ampDiffuse    = (__AMPLITUDE__FACTOR__SPARKLE__ / pow ( (distance(lightPos, FragPositionWorldSpace)) , 2.0)) * pointLightAmp;
        diffuseTotal += (vec4(pointLightColors[i], pointLightColors[i + 1], pointLightColors[i + 2], 0.0) * ampDiffuse);

        /*
        // specular
        vec3 fromLightSourceToFragpos = normalize(FragPositionWorldSpace - lightPos);
        vec3 fromFragposToCamera = normalize(LightPositionWorldSpace - FragPositionWorldSpace); // NOTE: reusing LightPositionWorldSpace because its position is identical to the camera
        float dot = dot(reflect(fromLightSourceToFragpos, GNormal ), fromFragposToCamera);
        float ampSpecular = pow(dot, 2.0) * float(dot > 0.0) * __AMPLITUDE__FACTOR__SPARKLE__ / pow ( (distance(lightPos, FragPositionWorldSpace)) , 2.0) * pointLightAmp;
        specularTotal += vec4(pointLightColors[i], pointLightColors[i + 1], pointLightColors[i + 2], 0.0) * ampSpecular;
        */
    }

    vec4 texel = texture2D(texture, texCoord);
    vec4 total = vec4(0.0, 0.0, 0.0, 1.0);

    total += texel * onCameraTotal;
    total += texel * diffuseTotal;
//    total += texel * specularTotal;

    gl_FragColor = total;

}























