//our attributes
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_texCoords;
attribute vec3 a_center;

//our camera matrix
uniform mat4 u_projTrans;


//send the color out to the fragment shader
varying vec3 fragPosition;
varying vec2 texCoord;

// our light
uniform vec3 LightPos;

// send the amplitude, precalculated in this shader, because it is the same for every pixel
varying float Amplitude;


void main() {


	gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);
	fragPosition = gl_Position.xyz;
	texCoord = a_texCoords;

    // precalculate amplitude for fragment shader because it's the same for every pixel
    float distanceFromLightToPictureCenter = distance(LightPos, a_center);
    distanceFromLightToPictureCenter -= __SIDE_LENGTH__.0;
    distanceFromLightToPictureCenter = max(distanceFromLightToPictureCenter, 0.0);
    Amplitude = 1.0 - (distanceFromLightToPictureCenter / __AMPLITUDE__FACTOR__);
}







