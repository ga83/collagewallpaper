#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texture;
varying vec2 texCoord;

// lighting variables
varying vec3 FragPositionWorldSpace;
varying vec3 LightPositionWorldSpace;


void main() {
    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPositionWorldSpace, FragPositionWorldSpace)) , 2.0);
    gl_FragColor = texture2D(texture, texCoord) * amplitude;
}
