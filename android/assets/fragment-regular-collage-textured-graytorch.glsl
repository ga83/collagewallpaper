#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texture;
varying vec2 texCoord;

// lighting variables
varying vec3 FragPositionWorldSpace;
varying vec3 LightPositionWorldSpace;


vec4 greyify(vec4 fragColor, float amplitude);


void main() {
    float amplitude = __AMPLITUDE__FACTOR__ / pow ( (distance(LightPositionWorldSpace, FragPositionWorldSpace)) , 2.0);
    gl_FragColor = texture2D(texture, texCoord);
    gl_FragColor = greyify(gl_FragColor, amplitude);
}


vec4 greyify(vec4 fragColor, float amplitude) {
    amplitude = amplitude * amplitude * amplitude;
    float max = max(max(fragColor.r, fragColor.g), fragColor.b);
    float colAmp  = 0.0 + amplitude;
    float greyAmp = 1.0 - amplitude;

    vec4 colorOut;
    colorOut.r = fragColor.r * colAmp + max * greyAmp;
    colorOut.g = fragColor.g * colAmp + max * greyAmp;
    colorOut.b = fragColor.b * colAmp + max * greyAmp;
    colorOut.a = 1.0;

    return colorOut;
}


