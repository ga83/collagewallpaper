// our attributes
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_texCoords;

// our camera matrix
uniform mat4 u_projTrans;

// send these variables to the fragment shader
varying vec3 fragPosition;
varying vec2 texCoord;


void main() {
	gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);
	fragPosition = gl_Position.xyz;
	texCoord = a_texCoords;
}
