#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texture;
varying vec2 texCoord;

// lighting variables
varying float Amplitude;


vec4 greyify(vec4 fragColor, float amplitude);


void main() {
    gl_FragColor = texture2D(texture, texCoord);
    gl_FragColor = greyify(gl_FragColor, max(Amplitude, 0.0));
}


vec4 greyify(vec4 fragColor, float amplitude) {
    amplitude = amplitude * amplitude * amplitude;
    float max = max(max(fragColor.r, fragColor.g), fragColor.b);
    float colAmp  = 0.0 + amplitude;
    float greyAmp = 1.0 - amplitude;

    vec4 colorOut;
    colorOut.r = fragColor.r * colAmp + max * greyAmp;
    colorOut.g = fragColor.g * colAmp + max * greyAmp;
    colorOut.b = fragColor.b * colAmp + max * greyAmp;
    colorOut.a = 1.0;

    return colorOut;
}

