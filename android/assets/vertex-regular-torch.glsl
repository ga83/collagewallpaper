//our attributes
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_normal;
attribute vec2 a_texCoords;

//our camera matrix
uniform mat4 u_projTrans;


//send the color out to the fragment shader
varying vec3 fragPosition;
varying vec2 texCoord;


// send the light
varying vec3 LightPosFromVertex;

// send the frag position untransformed
varying vec3 FragPositionWorldSpace;

// send the light untransformed
varying vec3 LightPositionWorldSpace;

// our light
uniform vec3 LightPos;


void main() {
	gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);

	fragPosition = gl_Position.xyz;

	texCoord = a_texCoords;

	// untransformed frag position for lighting
	FragPositionWorldSpace = a_position;

	// untransformed light position for lighting
	LightPositionWorldSpace = LightPos;
	
}
